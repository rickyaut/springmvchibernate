package com.journaldev.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.journaldev.spring.dao.PersonDAO;
import com.journaldev.spring.dao.QueryResult;
import com.journaldev.spring.model.Person;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonDAO personDAO;

	@Override
	@Transactional
	public void addPerson(Person p) {
		this.personDAO.addPerson(p);
	}

	@Override
	@Transactional
	public void updatePerson(Person p) {
		this.personDAO.updatePerson(p);
	}

	@Override
	@Transactional
	public List<Person> listPersons() {
		return this.personDAO.listPersons();
	}

	@Override
	@Transactional
	public Person getPersonById(String id) {
		return this.personDAO.getPersonById(id);
	}

	@Override
	@Transactional
	public void removePerson(String id) {
		this.personDAO.removePerson(id);
	}

	@Override
	@Transactional
	public QueryResult<List<Object>> countByCountry(int startPosition, int maxResults) {
		return this.personDAO.countByCountry(startPosition, maxResults);
	}

	@Override
	public QueryResult<Person> findByPartOfName(int startPosition, int maxResults) {
		return personDAO.findByPartOfName(startPosition, maxResults);
	}

	@Override
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry(int startPosition, int maxResults) {
		return personDAO.findByPartOfNameThenCountByCountry(startPosition, maxResults);
	}

	@Override
	public QueryResult<Person> findByPartOfName2(int startPosition, int maxResults) {
		return personDAO.findByPartOfName2(startPosition, maxResults);
	}

	@Override
	public QueryResult<List<Object>> countByCountry2(int startPosition, int maxResults) {
		return personDAO.countByCountry2(startPosition, maxResults);
	}

	@Override
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry2(int startPosition, int maxResults) {
		return personDAO.findByPartOfNameThenCountByCountry2(startPosition, maxResults);
	}

	@Override
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry3(int startPosition, int maxResults) {
		return personDAO.findByPartOfNameThenCountByCountry3(startPosition, maxResults);
	}

}
