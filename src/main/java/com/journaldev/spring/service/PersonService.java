package com.journaldev.spring.service;

import java.util.List;

import com.journaldev.spring.dao.QueryResult;
import com.journaldev.spring.model.Person;

public interface PersonService {

	public void addPerson(Person p);
	public void updatePerson(Person p);
	public List<Person> listPersons();
	public Person getPersonById(String id);
	public void removePerson(String id);
	public QueryResult<List<Object>> countByCountry(int startPosition, int maxResults);
	public QueryResult<Person> findByPartOfName(int startPosition, int maxResults);
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry(int startPosition, int maxResults);
	public QueryResult<Person> findByPartOfName2(int startPosition, int maxResults);
	public QueryResult<List<Object>> countByCountry2(int startPosition, int maxResults);
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry2(int startPosition, int maxResults);
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry3(int startPosition, int maxResults);
	
}
