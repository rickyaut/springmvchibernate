package com.journaldev.spring;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.journaldev.spring.model.Person;
import com.journaldev.spring.service.PersonService;

@Controller
public class PersonController {
	
	private PersonService personService;
	
	@Autowired(required=true)
	@Qualifier(value="personService")
	public void setPersonService(PersonService ps){
		this.personService = ps;
	}
	
	@RequestMapping(value = "/persons", method = RequestMethod.GET)
	public String listPersons(Model model) {
		model.addAttribute("person", new Person());
		model.addAttribute("listPersons", personService.listPersons());
		model.addAttribute("findByPartOfNameResult", personService.findByPartOfName(0, 0));
		model.addAttribute("findByPartOfNameResult_Paging", personService.findByPartOfName(1, 3));
        model.addAttribute("countByCountryResult", personService.countByCountry(0, 0));
        model.addAttribute("countByCountryResult_Paging", personService.countByCountry(1, 3));
        model.addAttribute("findByPartOfName_countByCountryResult", personService.findByPartOfNameThenCountByCountry(0, 0));
        model.addAttribute("findByPartOfName_countByCountryResult_Paging", personService.findByPartOfNameThenCountByCountry(1, 3));
		model.addAttribute("findByPartOfNameResult2", personService.findByPartOfName2(0, 0));
		model.addAttribute("findByPartOfNameResult2_Paging", personService.findByPartOfName2(1, 3));
        model.addAttribute("countByCountryResult2", personService.countByCountry2(0, 0));
        model.addAttribute("countByCountryResult2_Paging", personService.countByCountry2(1, 3));
        model.addAttribute("findByPartOfName_countByCountryResult2", personService.findByPartOfNameThenCountByCountry2(0, 0));
        model.addAttribute("findByPartOfName_countByCountryResult2_Paging", personService.findByPartOfNameThenCountByCountry2(1, 3));
        model.addAttribute("findByPartOfName_countByCountryResult3", personService.findByPartOfNameThenCountByCountry3(0, 0));
        model.addAttribute("findByPartOfName_countByCountryResult3_Paging", personService.findByPartOfNameThenCountByCountry3(1, 3));
		return "person";
	}
	
	//For add and update person both
	@RequestMapping(value= "/person/add", method = RequestMethod.POST)
	public String addPerson(@ModelAttribute("person") Person p){
		
		if(StringUtils.isBlank(p.getId())){
			//new person, add it
			this.personService.addPerson(p);
		}else{
			//existing person, call update
			this.personService.updatePerson(p);
		}
		
		return "redirect:/persons";
		
	}
	
	@RequestMapping("/remove/{id}")
    public String removePerson(@PathVariable("id") String id){
		
        this.personService.removePerson(id);
        return "redirect:/persons";
    }
 
    @RequestMapping("/edit/{id}")
    public String editPerson(@PathVariable("id") String id, Model model){
        model.addAttribute("person", this.personService.getPersonById(id));
        return "person";
    }
	
}
