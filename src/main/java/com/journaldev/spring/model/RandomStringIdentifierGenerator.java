package com.journaldev.spring.model;

import java.io.Serializable;
import java.util.Properties;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.type.Type;

public class RandomStringIdentifierGenerator implements IdentifierGenerator, Configurable {

	private static final Log logger = LogFactory.getLog(RandomStringIdentifierGenerator.class);
	@Override
	public void configure(Type type, Properties params, Dialect dialect) throws MappingException {
		logger.debug("configuring RandomStringIdentifierGenerator");
	}

	@Override
	public Serializable generate(SessionImplementor session, Object obj)
			throws HibernateException {
		Session hibernateSession = session.getFactory().getCurrentSession();
		Transaction transaction = null;
		try{
			transaction = hibernateSession.beginTransaction();
			for(;;){
				String key = StringUtils.upperCase(String.format("%s-%s-%s", RandomStringUtils.randomAlphabetic(3), RandomStringUtils.randomAlphabetic(3), RandomStringUtils.randomNumeric(3)));
				if( hibernateSession.get(obj.getClass(), key) == null){
					return key;
				}
			}
		}finally{
			if(transaction != null){
				transaction.commit();
			}
		}
		
	}
}