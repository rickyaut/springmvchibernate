package com.journaldev.spring.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.GenericGenerator;

/**
 * Entity bean with JPA annotations
 * Hibernate provides JPA implementation
 * @author Ricky
 *
 */
@Entity(name="Person")
@NamedQueries({@NamedQuery(name="Person.findByPartOfName", query="SELECT p FROM Person p WHERE LOWER(name) LIKE CONCAT('%', LOWER(:name), '%') order by p.name asc"),
	@NamedQuery(name="Person.findByPartOfName_countByCountry", query="SELECT country, COUNT(*) FROM Person p WHERE LOWER(name) LIKE CONCAT('%', LOWER(:name), '%') GROUP BY country order by count(*) desc"),
	@NamedQuery(name="Person.countByCountry", query="SELECT country, COUNT(*) FROM Person p GROUP BY country order by country desc")})
public class Person implements Serializable{

    @Id
    @GenericGenerator(name = "person-sequence", strategy = "com.journaldev.spring.model.RandomStringIdentifierGenerator")
    @GeneratedValue(generator = "person-sequence", strategy = GenerationType.SEQUENCE)
	private String id;
	
	private String name;
	
	private String country;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@Override
	public String toString(){
		return "id="+id+", name="+name+", country="+country;
	}
}
