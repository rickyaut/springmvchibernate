package com.journaldev.spring.dao;

public class QueryOverSizeException extends Exception {
	public static final int QUERY_SIZE_LIMIT = 1000;

}
