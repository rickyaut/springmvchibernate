package com.journaldev.spring.dao;

import java.io.Serializable;

public interface JpaDAO<T extends Serializable> {

	/**
	 * save an entity
	 * @param entity entity to be saved
	 */
	public abstract void save(T entity);

	/**
	 * update an entity
	 * @param entity entity to be updated
	 */
	public abstract void update(T entity);

	/**
	 * remove entity
	 * @param entity entity to be removed
	 */
	public abstract void remove(T entity);

	/**
	 * remove an entity identified by id
	 * @param id String primary key of the entity to be removed
	 */
	public abstract void remove(String id);

	/**
	 * load entity by id
	 * @param id id of the entity
	 * @return matching entity or null
	 */
	public abstract T get(String id);
	
	/**
	 * find all entities belong to specified country(case ignored)
	 * @param country the specified country code
	 * @return all matching entities
	 * @throws QueryOverSizeException 
	 */
	public abstract QueryResult<T> findAllByCountry(String country) throws QueryOverSizeException;

	/**
	 * find all entities of type T
	 * @return all entities of type T
	 * @throws QueryOverSizeException 
	 */
	public abstract QueryResult<T> findAll() throws QueryOverSizeException;



}