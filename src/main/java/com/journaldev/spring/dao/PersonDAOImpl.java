package com.journaldev.spring.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.journaldev.spring.model.Person;

@Repository
public class PersonDAOImpl extends AbstractJpaDAO<Person> implements PersonDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(PersonDAOImpl.class);

	@Override
	public void addPerson(Person p) {
		save(p);
		logger.info("Person saved successfully, Person Details="+p);
	}

	@Override
	public void updatePerson(Person p) {
		update(p);
		logger.info("Person updated successfully, Person Details="+p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Person> listPersons() {
		List<Person> personsList = entityManager.createQuery("from " + Person.class.getName(), Person.class).getResultList();
		return personsList;
	}

	@Override
	public Person getPersonById(String id) {
		Person p = get(id);
		logger.info("Person loaded successfully, Person details="+p);
		return p;
	}

	@Override
	public void removePerson(String id) {
		remove(id);
	}

	@Override
	public QueryResult<List<Object>> countByCountry(int startPosition, int maxResults){
		return queryByNamedQuery("Person.countByCountry", null, startPosition, maxResults);
	}

	@Override
	public QueryResult<Person> findByPartOfName(int startPosition, int maxResults) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("name", "a");
		return searchByNamedQuery("Person.findByPartOfName", parameters, startPosition, maxResults);
	}

	@Override
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry(int startPosition, int maxResults) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("name", "a");
		return queryByNamedQuery("Person.findByPartOfName_countByCountry", parameters, startPosition, maxResults);
	}

	@Override
	public QueryResult<Person> findByPartOfName2(int startPosition, int maxResults) {
		EntityType<Person> type = entityManager.getMetamodel().entity(Person.class);
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);
		Root<Person> from = criteriaQuery.from(Person.class);
		Predicate where = criteriaBuilder.like(criteriaBuilder.lower(from.get(type.getSingularAttribute("name", String.class))), "%a%");
		criteriaQuery.where(where);
		criteriaQuery.orderBy(criteriaBuilder.asc(from.get(type.getSingularAttribute("name"))));
		CriteriaQuery<Person> select = criteriaQuery.select(from);
		try {
			return searchByCriteriaQuery(criteriaQuery, startPosition, maxResults);
		} catch (QueryOverSizeException e) {
			logger.error("too many results returned from one query", e);
			return null;
		}
	}

	@Override
	public QueryResult<List<Object>> countByCountry2(int startPosition, int maxResults) {
		EntityType<Person> type = entityManager.getMetamodel().entity(Person.class);
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
		Root<Person> from = criteriaQuery.from(Person.class);
		criteriaQuery.multiselect(from.get("country"), criteriaBuilder.count(from)).groupBy(from.get("country"));
		criteriaQuery.orderBy(criteriaBuilder.asc(criteriaBuilder.count(from)));
		return queryByCriteriaQuery(criteriaQuery, startPosition, maxResults);
	}

	@Override
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry2(int startPosition, int maxResults) {
		EntityType<Person> type = entityManager.getMetamodel().entity(Person.class);
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
		Root<Person> from = criteriaQuery.from(Person.class);
		Predicate where = criteriaBuilder.like(criteriaBuilder.lower(from.get(type.getSingularAttribute("name", String.class))), "%a%");
		criteriaQuery.where(where);
		criteriaQuery.multiselect(from.get("country"), criteriaBuilder.count(from)).groupBy(from.get("country"));
		return queryByCriteriaQuery(criteriaQuery, startPosition, maxResults);
	}

	@Override
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry3(int startPosition, int maxResults) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("name", "a");
		return queryByHQL("SELECT country, COUNT(*) FROM Person p WHERE LOWER(name) LIKE CONCAT('%', LOWER(:name), '%') GROUP BY country order by count(*) desc", parameters, startPosition, maxResults);
	}

}
