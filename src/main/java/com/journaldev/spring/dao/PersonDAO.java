package com.journaldev.spring.dao;

import java.util.List;

import com.journaldev.spring.model.Person;

public interface PersonDAO extends JpaDAO<Person>{

	public void addPerson(Person p);
	public void updatePerson(Person p);
	public List<Person> listPersons();
	public Person getPersonById(String id);
	public void removePerson(String id);
	public QueryResult<List<Object>> countByCountry(int startPosition, int maxResults);
	public QueryResult<Person> findByPartOfName(int startPosition, int maxResults);
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry(int startPosition, int maxResults);
	public QueryResult<Person> findByPartOfName2(int startPosition, int maxResults);
	public QueryResult<List<Object>> countByCountry2(int startPosition, int maxResults);
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry2(int startPosition, int maxResults);
	public QueryResult<List<Object>> findByPartOfNameThenCountByCountry3(int startPosition, int maxResults);
}
