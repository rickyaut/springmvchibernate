package com.journaldev.spring.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.apache.commons.lang.StringUtils;
import org.hibernate.engine.TypedValue;
import org.hibernate.impl.AbstractQueryImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class AbstractJpaDAO<T extends Serializable> implements JpaDAO<T> {
	private static final Logger logger = LoggerFactory.getLogger(AbstractJpaDAO.class);

	@PersistenceContext
	protected EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	private Class<T> getTypeParameterClass(){
        Type type = getClass().getGenericSuperclass();
        ParameterizedType paramType = (ParameterizedType) type;
        return (Class<T>) paramType.getActualTypeArguments()[0];
    }
	
	private Object getField(final Object object, final Class<?> clazz, final String fieldName) {
        try {
            final Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(object);
        } catch (final ReflectiveOperationException e) {
            throw new IllegalArgumentException(e);
        }
    }
	
	private void resolveQueryParameters(Query q, Map<String, Object> parameters, int startPosition, int maxResults) {
		if(parameters != null){
			for(Map.Entry<String, Object> parameter : parameters.entrySet()){
				q.setParameter(parameter.getKey(), parameter.getValue());
			}
		}
		q.setFirstResult(startPosition > 0? startPosition : 0);
		if(maxResults > 0){
			q.setMaxResults(maxResults);
		}
	}

	private int countQueryResult(Query query, Map<String, Object> parameters){
		String sql = query.unwrap(org.hibernate.Query.class).getQueryString();
		Query countQuery = entityManager.createNativeQuery(String.format("select count(*) from (%s) as subquery", sql));
		if(parameters != null){
			for(Map.Entry<String, Object> parameter : parameters.entrySet()){
				countQuery.setParameter(parameter.getKey(), parameter.getValue());
			}
		}else{
			AbstractQueryImpl hibernateQuery = query.unwrap(AbstractQueryImpl.class);
			Map<String, TypedValue> pNamedParameters = (Map<String, TypedValue>) getField(hibernateQuery, AbstractQueryImpl.class, "namedParameters");
			for(Map.Entry<String, TypedValue> parameter : pNamedParameters.entrySet()){
				countQuery.setParameter(parameter.getKey(), parameter.getValue().getValue());
			}

		}
		return ((Number)countQuery.getSingleResult()).intValue();
	}
	
	/* (non-Javadoc)
	 * @see com.journaldev.spring.dao.JpaDAO#save(T)
	 */
	@Override
	public void save(T entity){
		entityManager.persist(entity);
	}
	
	/* (non-Javadoc)
	 * @see com.journaldev.spring.dao.JpaDAO#update(T)
	 */
	@Override
	public void update(T entity){
		entityManager.merge(entity);
	}
	
	/* (non-Javadoc)
	 * @see com.journaldev.spring.dao.JpaDAO#remove(T)
	 */
	@Override
	public void remove(T entity){
		entityManager.remove(entity);
	}
	
	/* (non-Javadoc)
	 * @see com.journaldev.spring.dao.JpaDAO#remove(java.lang.String)
	 */
	@Override
	public void remove(String id){
		T entity = get(id);
		entityManager.remove(entity);
	}
	
	/* (non-Javadoc)
	 * @see com.journaldev.spring.dao.JpaDAO#get(java.lang.String)
	 */
	@Override
	public T get(String id){
		return entityManager.find(getTypeParameterClass(), id);
	}
	
	/* (non-Javadoc)
	 * @see com.journaldev.spring.dao.JpaDAO#findAll()
	 */
	@Override
	public QueryResult<T> findAll() throws QueryOverSizeException{
		return findAllByCountry(null);
	}
	
	/* (non-Javadoc)
	 * @see com.journaldev.spring.dao.JpaDAO#findAllByCountry(java.lang.String)
	 */
	@Override
	public QueryResult<T> findAllByCountry(String country) throws QueryOverSizeException{
		EntityType<T> type = entityManager.getMetamodel().entity(getTypeParameterClass());
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(getTypeParameterClass());
		Root<T> from = criteriaQuery.from(getTypeParameterClass());
		if(StringUtils.isNotBlank(country)){
			Predicate where = criteriaBuilder.equal(criteriaBuilder.lower(from.get(type.getSingularAttribute("country", String.class))), country);
			criteriaQuery.where(where);
		}
		CriteriaQuery<T> select = criteriaQuery.select(from);
		return searchByCriteriaQuery(criteriaQuery);
	}
	
	/**
	 * search table to find a matching entity by using NamedQuery
	 * @param qName name of the NamedQuery
	 * @param parameters parameters for the NamedQuery, it could be null or empty when there is no parameters
	 * @return return matching entity or null
	 */
	protected T getByNamedQuery(String qName, Map<String, Object> parameters ){
		Query q = entityManager.createNamedQuery(qName);
		resolveQueryParameters(q, parameters, 0, 1);
		try{
			return (T)q.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * search table to find a matching entity by using NamedQuery
	 * @param hql HQL
	 * @param parameters parameters for the NamedQuery, it could be null or empty when there is no parameters
	 * @return return matching entity or null
	 */
	protected T getByHQL(String hql, Map<String, Object> parameters ){
		Query q = entityManager.createQuery(hql, getTypeParameterClass());
		resolveQueryParameters(q, parameters, 0, 1);
		try{
			return (T)q.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}
	
	/**
	 * search table to find a matching entity by using Criteria
	 * @param criteriaQuery criteria for matching entities
	 * @return return matching entity or null
	 */
	protected T getByCriteriaQuery(CriteriaQuery<T> criteriaQuery){
		QueryResult<T> queryResults = new QueryResult<T>();
		TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
		try{
			return query.getSingleResult();
		}catch(NoResultException ex){
			return null;
		}
	}

	/**
	 * search table to find all matching entities by using NamedQuery
	 * @param qName name of the NamedQuery
	 * @param parameters parameters for the NamedQuery, it could be null or empty when there is no parameters
	 * @return return all matching results
	 */
	protected QueryResult<T> searchByNamedQuery(String qName, Map<String, Object> parameters ){
		return searchByNamedQuery(qName, parameters, 0, 1);
	}
	
	/**
	 * search table to find page-size matching entities by using NamedQuery
	 * @param qName name of the NamedQuery
	 * @param parameters parameters for the NamedQuery, it could be null or empty when there is no parameters
	 * @param startPosition position of the first returned entity in the matching resultset
	 * @param maxResults maximum entities returned in this query
	 * @return return page-size matching results
	 */
	protected QueryResult<T> searchByNamedQuery(String qName, Map<String, Object> parameters, int startPosition, int maxResults){
		QueryResult<T> queryResults = new QueryResult<T>();
		
		Query q = entityManager.createNamedQuery(qName);
		queryResults.setCount(countQueryResult(q, parameters));
		resolveQueryParameters(q, parameters, startPosition, maxResults);
		queryResults.setResult(q.getResultList());
		return queryResults;
	}
	
	/**
	 * search table to find all matching entities by using NamedQuery
	 * @param hql HQL
	 * @param parameters parameters for the NamedQuery, it could be null or empty when there is no parameters
	 * @return return all matching results
	 * @throws QueryOverSizeException 
	 */
	protected QueryResult<T> searchByHQL(String hql, Map<String, Object> parameters ) throws QueryOverSizeException{
		return searchByHQL(hql, parameters, 0, 1);
	}
	
	/**
	 * search table to find page-size matching entities by using NamedQuery
	 * @param hql HQL
	 * @param parameters parameters for the NamedQuery, it could be null or empty when there is no parameters
	 * @param startPosition position of the first returned entity in the matching resultset
	 * @param maxResults maximum entities returned in this query
	 * @return return page-size matching results
	 * @throws QueryOverSizeException 
	 */
	protected QueryResult<T> searchByHQL(String hql, Map<String, Object> parameters, int startPosition, int maxResults) throws QueryOverSizeException{
		QueryResult<T> queryResults = new QueryResult<T>();
		
		Query q = entityManager.createQuery(hql);
		queryResults.setCount(countQueryResult(q, parameters));
		if (queryResults.getCount() > QueryOverSizeException.QUERY_SIZE_LIMIT
				&& (maxResults > QueryOverSizeException.QUERY_SIZE_LIMIT || maxResults == 0)) {
			throw new QueryOverSizeException();
		}

		resolveQueryParameters(q, parameters, startPosition, maxResults);
		queryResults.setResult(q.getResultList());
		return queryResults;
	}
	
	/**
	 * search table to find all matching entities by using Criteria
	 * @param criteriaQuery criteria for matching entities
	 * @return return all matching results
	 * @throws QueryOverSizeException 
	 */
	protected QueryResult<T> searchByCriteriaQuery(CriteriaQuery<T> criteriaQuery) throws QueryOverSizeException{
		return searchByCriteriaQuery(criteriaQuery, 0, 0);
	}
	
	/**
	 * search table to find page-size matching entities by using NamedQuery
	 * @param criteriaQuery criteria for matching entities
	 * @param startPosition position of the first returned entity in the matching resultset
	 * @param maxResults maximum entities returned in this query
	 * @return return page-size matching results
	 * @throws QueryOverSizeException 
	 */
	protected QueryResult<T> searchByCriteriaQuery(CriteriaQuery<T> criteriaQuery, int startPosition, int maxResults) throws QueryOverSizeException{
		QueryResult<T> queryResults = new QueryResult<T>();
		TypedQuery<T> query = entityManager.createQuery(criteriaQuery);
		queryResults.setCount(countQueryResult(query, null));
		if (queryResults.getCount() > QueryOverSizeException.QUERY_SIZE_LIMIT
				&& (maxResults > QueryOverSizeException.QUERY_SIZE_LIMIT || maxResults == 0)) {
			throw new QueryOverSizeException();
		}
		query.setFirstResult(startPosition > 0? startPosition : 0);
		if(maxResults > 0){
			query.setMaxResults(maxResults);
		}
		queryResults.setResult(query.getResultList());

		return queryResults;
	}

	/**
	 * query entities and return properties/projections by using NamedQuery
	 * @param qName name of the NamedQuery
	 * @param nullable parameters parameters for the NamedQuery
	 * @return all matching results
	 */
	protected QueryResult<List<Object>> queryByNamedQuery(String qName, Map<String, Object> parameters ){
		return queryByNamedQuery(qName, parameters, 0, 0);
	}
	
	/**
	 * query entities and return page-size properties/projections by using NamedQuery
	 * @param qName name of the NamedQuery
	 * @param nullable parameters parameters for the NamedQuery
	 * @param startPosition position of the first returned row in the matching resultset
	 * @param maxResults maximum rows returned in this query
	 * @return all matching results
	 */
	protected QueryResult<List<Object>> queryByNamedQuery(String qName, Map<String, Object> parameters, int startPosition, int maxResults){
		QueryResult<List<Object>> queryResults = new QueryResult<List<Object>>();
		Query q1 = entityManager.createNamedQuery(qName);
		queryResults.setCount(countQueryResult(q1, parameters));
		resolveQueryParameters(q1, parameters, startPosition, maxResults);
		queryResults.setResult(q1.getResultList());
		return queryResults;
	}
	
	/**
	 * query entities and return properties/projections by using NamedQuery
	 * @param hql HQL
	 * @param nullable parameters parameters for the NamedQuery
	 * @return all matching results
	 */
	protected QueryResult<List<Object>> queryByHQL(String hql, Map<String, Object> parameters ){
		return queryByHQL(hql, parameters, 0, 0);
	}
	
	/**
	 * query entities and return page-size properties/projections by using NamedQuery
	 * @param hql HQL
	 * @param nullable parameters parameters for the NamedQuery
	 * @param startPosition position of the first returned row in the matching resultset
	 * @param maxResults maximum rows returned in this query
	 * @return all matching results
	 */
	protected QueryResult<List<Object>> queryByHQL(String hql, Map<String, Object> parameters, int startPosition, int maxResults){
		QueryResult<List<Object>> queryResults = new QueryResult<List<Object>>();
		Query q1 = entityManager.createQuery(hql);
		queryResults.setCount(countQueryResult(q1, parameters));
		resolveQueryParameters(q1, parameters, startPosition, maxResults);
		queryResults.setResult(q1.getResultList());
		return queryResults;
	}
	
	/**
	 * query entities and return properties/projections by using NamedQuery
	 * @param criteriaQuery criteria for the query
	 * @return all matching results
	 */
	protected QueryResult<List<Object>> queryByCriteriaQuery(CriteriaQuery<Tuple> criteriaQuery){
		return queryByCriteriaQuery(criteriaQuery, 0, 0);
	}
	
	/**
	 * query entities and return page-size properties/projections by using NamedQuery
	 * @param criteriaQuery criteria for the query
	 * @param startPosition position of the first returned row in the matching resultset
	 * @param maxResults maximum rows returned in this query
	 * @return all matching results
	 */
	protected QueryResult<List<Object>> queryByCriteriaQuery(CriteriaQuery<Tuple> criteriaQuery, int startPosition, int maxResults){
		QueryResult<List<Object>> queryResults = new QueryResult<List<Object>>();
		TypedQuery<Tuple> query = entityManager.createQuery(criteriaQuery);
		queryResults.setCount(countQueryResult(query, null));
		
		query.setFirstResult(startPosition);
		if(maxResults > 0){
			query.setMaxResults(maxResults);
		}
		List<Tuple> resultTupleList = query.getResultList();
		List<List<Object>> resultListList = new ArrayList<List<Object>>();
		for(Tuple tuple : resultTupleList){
			resultListList.add(Arrays.asList(tuple.toArray()));
		}
		queryResults.setResult(resultListList);

		return queryResults;
	}
}
