package com.journaldev.spring.dao;

import java.util.List;

public class QueryResult<T> {
	private int count;
	private List<T> result;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<T> getResult() {
		return result;
	}

	public void setResult(List<T> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "QueryResult [count=" + count + ", result=" + result + "]";
	}
}
