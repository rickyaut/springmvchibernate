<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Person Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a Person
</h1>

<c:url var="addAction" value="/person/add" ></c:url>

<form:form action="${addAction}" commandName="person">
<table>
	<c:if test="${!empty person.name}">
	<tr>
		<td>
			<form:label path="id">
				<spring:message text="ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="id" readonly="true" size="8"  disabled="true" />
			<form:hidden path="id" />
		</td> 
	</tr>
	</c:if>
	<tr>
		<td>
			<form:label path="name">
				<spring:message text="Name"/>
			</form:label>
		</td>
		<td>
			<form:input path="name" />
		</td> 
	</tr>
	<tr>
		<td>
			<form:label path="country">
				<spring:message text="Country"/>
			</form:label>
		</td>
		<td>
			<form:input path="country" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<c:if test="${!empty person.name}">
				<input type="submit"
					value="<spring:message text="Edit Person"/>" />
			</c:if>
			<c:if test="${empty person.name}">
				<input type="submit"
					value="<spring:message text="Add Person"/>" />
			</c:if>
		</td>
	</tr>
</table>	
</form:form>
<br>
<h3>Persons List</h3>
<c:if test="${!empty listPersons}">
	<table class="tg">
	<tr>
		<th width="80">Person ID</th>
		<th width="120">Person Name</th>
		<th width="120">Person Country</th>
		<th width="60">Edit</th>
		<th width="60">Delete</th>
	</tr>
	<c:forEach items="${listPersons}" var="person">
		<tr>
			<td>${person.id}</td>
			<td>${person.name}</td>
			<td>${person.country}</td>
			<td><a href="<c:url value='/edit/${person.id}' />" >Edit</a></td>
			<td><a href="<c:url value='/remove/${person.id}' />" >Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>findByPartOfName: name contains a/A</h3>
<c:if test="${!empty findByPartOfNameResult}">
	<div>Total findByPartOfNameResult: ${findByPartOfNameResult.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Name</th>
		<th width="60">Country</th>
	</tr>
	<c:forEach items="${findByPartOfNameResult.result}" var="result">
		<tr>
			<td>${result.name}</td>
			<td>${result.country}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>findByPartOfName_Paging: name contains a/A</h3>
<c:if test="${!empty findByPartOfNameResult_Paging}">
	<div>Total findByPartOfNameResult_Paging: ${findByPartOfNameResult_Paging.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Name</th>
		<th width="60">Country</th>
	</tr>
	<c:forEach items="${findByPartOfNameResult_Paging.result}" var="result">
		<tr>
			<td>${result.name}</td>
			<td>${result.country}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>use Criteria to findByPartOfName: name contains a/A</h3>
<c:if test="${!empty findByPartOfNameResult2}">
	<div>Total findByPartOfNameResult: ${findByPartOfNameResult2.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Name</th>
		<th width="60">Country</th>
	</tr>
	<c:forEach items="${findByPartOfNameResult2.result}" var="result">
		<tr>
			<td>${result.name}</td>
			<td>${result.country}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>use Criteria to findByPartOfName_Paging: name contains a/A</h3>
<c:if test="${!empty findByPartOfNameResult2_Paging}">
	<div>Total findByPartOfNameResult_Paging: ${findByPartOfNameResult2_Paging.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Name</th>
		<th width="60">Country</th>
	</tr>
	<c:forEach items="${findByPartOfNameResult2_Paging.result}" var="result">
		<tr>
			<td>${result.name}</td>
			<td>${result.country}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>Count by country</h3>
<c:if test="${!empty countByCountryResult}">
	<div>Total countByCountryResult: ${countByCountryResult.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${countByCountryResult.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>Count by country Paging</h3>
<c:if test="${!empty countByCountryResult_Paging}">
	<div>Total countByCountryResult_Paging: ${countByCountryResult_Paging.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${countByCountryResult_Paging.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>Use Criteria to Count by country</h3>
<c:if test="${!empty countByCountryResult2}">
	<div>Total countByCountryResult: ${countByCountryResult2.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${countByCountryResult2.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>Use Criteria to Count by country Paging</h3>
<c:if test="${!empty countByCountryResult2_Paging}">
	<div>Total countByCountryResult_Paging: ${countByCountryResult2_Paging.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${countByCountryResult2_Paging.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>findByPartOfName: name contains a/A & Count by country</h3>
<c:if test="${!empty findByPartOfName_countByCountryResult}">
	<div>Total findByPartOfName_countByCountryResult: ${findByPartOfName_countByCountryResult.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${findByPartOfName_countByCountryResult.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>findByPartOfName: name contains a/A & Count by country Paging</h3>
<c:if test="${!empty findByPartOfName_countByCountryResult_Paging}">
	<div>Total findByPartOfName_countByCountryResult_Paging: ${findByPartOfName_countByCountryResult_Paging.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${findByPartOfName_countByCountryResult_Paging.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>use Criteria to findByPartOfName: name contains a/A & Count by country</h3>
<c:if test="${!empty findByPartOfName_countByCountryResult2}">
	<div>Total findByPartOfName_countByCountryResult: ${findByPartOfName_countByCountryResult2.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${findByPartOfName_countByCountryResult2.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>use Criteria to findByPartOfName: name contains a/A & Count by country Paging</h3>
<c:if test="${!empty findByPartOfName_countByCountryResult2_Paging}">
	<div>Total findByPartOfName_countByCountryResult_Paging: ${findByPartOfName_countByCountryResult2_Paging.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${findByPartOfName_countByCountryResult2_Paging.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>use HQL to findByPartOfName: name contains a/A & Count by country</h3>
<c:if test="${!empty findByPartOfName_countByCountryResult3}">
	<div>Total findByPartOfName_countByCountryResult: ${findByPartOfName_countByCountryResult3.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${findByPartOfName_countByCountryResult3.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

<br>
<h3>use HQL to findByPartOfName: name contains a/A & Count by country Paging</h3>
<c:if test="${!empty findByPartOfName_countByCountryResult3_Paging}">
	<div>Total findByPartOfName_countByCountryResult_Paging: ${findByPartOfName_countByCountryResult3_Paging.count }</div>
	<table class="tg">
	<tr>
		<th width="120">Person Country</th>
		<th width="60">Count</th>
	</tr>
	<c:forEach items="${findByPartOfName_countByCountryResult3_Paging.result}" var="result">
		<tr>
			<td>${result[0]}</td>
			<td>${result[1]}</td>
		</tr>
	</c:forEach>
	</table>
</c:if>

</body>
</html>
